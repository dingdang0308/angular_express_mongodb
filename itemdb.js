var mongoose = require('mongoose');

Schema   = mongoose.Schema;

var Item = new Schema({
        item: String,
        details: { model: String, manufacturer: String },
        stock: [ { size: String, qty: Number } ],
        category: String
      });
mongoose.model('Item', Item);