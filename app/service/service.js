angular.module('appStart')
	.service('httpService', function($http, $q) {

		return ({
			httpHelper: httpHelper
		})

		function httpHelper(requestMethod, params) {
			var req = $http({
							method: requestMethod,
							url:'/api',
							params: params,
							headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						})
			return req;
		}

	})
	.factory('allData', [function() {
		return {
			name: 'Paul'
		};
	}])