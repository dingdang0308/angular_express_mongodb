angular.module('appStart',
 ['ui.router',
 'appStart.controller',
 'todolist.controller',
 'notification.controller',
 'ngAnimate',
 'test.controller'
 ])

.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/app/todoList')

	$stateProvider
		//主頁
    .state('app', {
		abstract: true,
		url: '/app',
		templateUrl: 'template/main/main.html',
		controller: 'appCtrl'
    })

    .state('app.todoList', {
		url: '/todoList',
		views: {
			'content':{
				templateUrl: 'template/todolist.html',
				controller: 'todoListCtrl'
			}
		},
		//resolve: resolveController('../app/controller/todolist.controller.js'),
	})

	.state('app.notifocation', {
		url: '/notifocation',
		views: {
			'content':{
				templateUrl: 'template/notifocation.html',
				controller: 'notifocationCtrl'
			}
		}
	})

	.state('app.csstraining', {
		url: '/csstraining',
		views: {
			'content': {
				templateUrl: 'template/csstraining.html'
			}
		}
	})

	.state('app.angulargrid', {
		url: '/angulargrid',
		views: {
			'content': {
				templateUrl: 'template/angulargrid.html'
			}
		}
	})

	.state('app.controllertest', {
		url: '/controllertest',
		views: {
			'content': {
				templateUrl: 'template/controllertest.html'
			}
		}
	})

})
