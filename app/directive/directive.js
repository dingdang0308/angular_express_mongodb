angular.module('appStart')
	.directive('addbuttons', function($compile){
		// Runs during compile
		return {
			// name: '',
			// priority: 1,
			// terminal: true,
			// scope: {}, // {} = isolate, true = child, false/undefined = no change
			// controller: function($scope, $element, $attrs, $transclude) {},
			// require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
			restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
			// template: '<button>Click to add buttons</button>'
			// templateUrl: '',
			// replace: true,
			// transclude: true,
			// compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
			link: function(scope, element) {
				
				var button = '<div style="float: right;">' +
								'<button class="btn-floating waves-effect waves-light yellow darken-1" ng-click="editList(list)">' +
									'<i class="mdi-editor-mode-edit"></i>' +
								'</button>' +
								'<button class="btn-floating waves-effect waves-light red accent-2" ng-click="deleteList(list)">' +
									'<i class="mdi-action-delete"></i>' +
								'</button>' +
							'</div>';
				element.bind("click", function(e){
					var eid;
					//console.log($('#eid').val());
					//if is same elemants han return
					if (e.currentTarget.id === $('#eid').val()) { return };

					if ($('#eid').val() != e.target.id) {
						//remove elements
						console.log(e);
						$('#'+$('#eid').val())
				        	.children().eq(1).children().remove();
						//scope.$destroy();
						$('#eid').val('')
						$('#eid').val(e.target.id)
						$('#'+e.target.id)
				          	.children().eq(1)
				          	.append($compile(button)(scope));
				        console.log('不一樣')  	
					}else{
						console.log('一樣')
					}
		       	});
			}
		};
	})
