angular.module('test.controller', [])
	.controller('ParentCtrl', ['$scope', 'allData', function($scope, allData) {
		$scope.$on('to-parent', function(e, d) {
			console.log('不關我的事才怪', allData.name);

		});

		console.log('Hi 我是ParentCtrl 我使用了' + allData.name)
	}])
	.controller('SelfCtrl', ['$scope', 'allData', function($scope, allData) {
		$scope.click = function () {
			//傳給小孩的內容
			$scope.$broadcast('to-child', '內容');
			allData.name = '我改變了allData得值'
			//傳給老爸的內容
			$scope.$emit('to-parent');
		}
		console.log('Hi SelfCtrl 我使用了' + allData.name)
	}])
	.controller('ChildCtrl', ['$scope', 'allData', function($scope, allData) {
		$scope.$on('to-parent', function(e, d) {
	    	console.log('I\' the child, I got it', d);
	  	});

	  	console.log('Hi ChildCtrl 我使用了' + allData.name)
	}])
	.controller('BroCtrl', ['$scope', 'allData', function($scope, allData) {
		$scope.$on('to-parent', function(e, d) {
	    	console.log('不關我的事');
	  	});

	  	console.log('Hi BroCtrl 我使用了' + allData.name)
	}])
