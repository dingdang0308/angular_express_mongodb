angular.module('todolist.controller', [])
	.controller('todoListCtrl', ['$scope', '$http', 'httpService', function($scope, $http, httpService) {
		
		var obj1
		$scope.listActive = function(obj) {
			// if (obj1 != obj) {
			// 	obj1 = obj
			// 	console.log('not some');
			// }else{
			// 	console.log('some')
			// }
			var list = $('#' + obj.list._id)
			// var div = $('<div/>').css('float', 'right')
			// var editIcon = $('<i/>').addClass('mdi-editor-mode-edit');
			// var deleteIcon = $('<i/>').addClass('mdi-action-delete');
			// var editButton = $('<button/>').addClass('btn-floating waves-effect waves-light yellow darken-1').attr('ng-click', 'editList(list)');
			// var deleteButton = 	$('<button/>').addClass('btn-floating waves-effect waves-light red accent-2').attr('ng-click', 'deleteList(list)');
			list.addClass('active')
					.siblings().removeClass('active')
			// list.find('button').show();
			// list.siblings().find('button').hide();
			// $("#thisList").val(list)
		}

		var nowDate = new Date();
		$('.tooltipped').tooltip();

		// $('.modal-trigger').leanModal();
		$scope.addList = function() {
			//console.log($scope.listData);
				if ($scope.content) {
					httpService.httpHelper('POST', {content: $scope.content, date: nowDate})
						.then(function(data) {
						console.log(data)
						if (data.data.isSave) {
							$scope.listData.push({
								_id: data.data._id,
								content: $scope.content,
								date: nowDate
							})
							//console.log($scope.listData)
							Materialize.toast('新增成功', 1000)
							$scope.content = '';

						};
					})
				};
		}


		$scope.addMore = function() {
			
		}

		$scope.deleteList = function(item) {
			console.log(item);

			$http({
				method: 'DELETE',
				url:'/api',
				params: {id: item._id},
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data) {
				//$scope.listData.push(data)
				if (data.isDelete) {
					var index = $scope.listData.indexOf(item);
					//alert('刪除成功')
					Materialize.toast('刪除成功', 1000, 'rounded')
					$scope.listData.splice(index, 1)
				};
			})
		}

		$scope.editList = function(item) {
				$('#modal1').openModal();
				//$scope.editContent = item.content;
				angular.element($('#modal1')).scope().list = item;
				//console.log(angular.element($('#modal1')).scope().editContent)
		}

		$scope.save = function(item) {
			$http({
				method: 'PUT',
				url:'/api',
				params: {id: item._id, content: item.content},
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}).success(function(data) {
				//$scope.listData.push(data)
				if (data.isSave) {
					Materialize.toast('儲存成功', 1000, 'rounded')
					$('#modal1').closeModal();
					//先轉成date 給 filter format
					var date = new Date(data.date)
					$scope.list.date = date;
				}
			})
		}

	}]);











