var express = require('express');
path = require ('path');
http    = require ('http');
favicon = require('serve-favicon');
logger = require('morgan');
cookieParser = require('cookie-parser');
bodyParser = require('body-parser');
require('./testMongo');
require('./itemdb');


routes = require('./routes/index');
users = require('./routes/users');
api = require('./routes/api');
itemApi = require('./routes/itemApi'); 
app = express();
server = http.createServer(app);
console.log(__dirname + '/bower_components')
app.use(express.static(path.join(__dirname, 'bower_components')));
app.use(express.static(path.join(__dirname, 'assets')));
app.use(express.static(path.join(__dirname, 'app')));

app.use('', routes);
app.use('/users', users);
//接收再回拋
app.post('/api', api.create);
app.delete('/api', api.deleteList);
app.get('/api', api.getLists);
app.put('/api', api.updateList);
app.post('/api.item', itemApi.createItem);
server.listen(process.env.PORT || 3000);
server.on('listening', function() {
  console.log('Express server started on port %s at %s', server.address().port, server.address().address);
});
