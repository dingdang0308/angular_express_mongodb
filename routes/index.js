var express = require('express');
router = express.Router();

/* GET home page. */
router.get('', function(req, res, next) {
  //使用靜態網頁
  res.sendfile('./app/index.html')
});

module.exports = router;
