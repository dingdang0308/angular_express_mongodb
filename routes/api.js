// routes/api.js

// C
var mongoose = require('mongoose');
Todo = mongoose.model('Todo');

exports.create = function(req, res) {

	new Todo({
		content: req.param('content'),
		date: req.param('date'),
		checked: false 
	}).save(function(err, todo, count) {
		console.log('ERR::::::' + err);
		console.log('todo::::::' + todo);
		console.log('count::::::' + count);
		if (err) {
			console.log('ERR::::::' + err);
			res.json({
				isSave: false
			})
		};
		res.json({
			isSave: true,
			_id: todo._id
		})
	})
};


// R
exports.getLists = function(req, res) {
	Todo.find(function(err, data) {
		if (err) {
			return console.error(err);
		};
		res.json(data)
	})
}

// D
exports.deleteList = function(req, res) {
	Todo.remove({_id: req.param('id')}, function(err, removed, count) {
		if (err) {
			return console.error(err);
		};
		if (err) {
			res.json({
				isDelete: false
			})
			return;
		};
		if (removed) {
			// console.log(count);
			res.json({
				isDelete: true
			})
		}else {
			res.json({
				isDelete: false
			})
		}
	})
}

exports.updateList = function(req, res) {
	var date = Date();
	Todo.update({_id: req.param('id')}, {$set: {content: req.param('content'), date: date}}, function(err, todo) {
		if (err) {
			res.json({
				isSave: false
			})
		};
		res.json({
			isSave: true,
			date: date
		})
	})
}
