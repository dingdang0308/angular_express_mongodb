'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');
var notify = require('gulp-notify');
var growl = require('gulp-notify-growl');
var jscs = require('gulp-jscs');
var jshint = require('gulp-jshint');
var compass = require('gulp-compass');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');

var port = process.env.PORT || 3000;

// we'd need a slight delay to reload browsers
// connected to browser-sync after restarting nodemon
var BROWSER_SYNC_RELOAD_DELAY = 700;


//reload server
gulp.task('nodemon', function (cb) {
  var called = false;
  return nodemon({

    // nodemon our expressjs server
    script: 'server.js',

    // watch core server file(s) that require server restart on change
    watch: ['server.js', 'routes/*.js']
  })
    .on('start', function onStart() {
      // ensure start only got called once
      if (!called) { cb(); }
      called = true;
    })
    .on('restart', function onRestart() {
      // reload connected browsers after a slight delay
      setTimeout(function reload() {
        browserSync.reload({
          stream: false   //
        });
      }, BROWSER_SYNC_RELOAD_DELAY);
    });
});

//壓縮js
gulp.task('uglify', function () {
    gulp.src('app/controller/*.js')

      //合併文件
      .pipe(concat('main.js'))

      //壓縮文件
      .pipe(uglify())

      //儲存文件
      .pipe(gulp.dest('assets/js'))
})

//reload page and css

gulp.task('browser-sync', ['nodemon'], function () {

  // for more browser-sync config options: http://www.browsersync.io/docs/options/
  browserSync.init({

    // watch the following files; changes will be injected (css & images) or cause browser to refresh
    files: ['app/**/*.*', 'assets/css/*.*'],

    // informs browser-sync to proxy our expressjs app which would run at the following location
    proxy: 'http://localhost:' + port,

    // informs browser-sync to use the following port for the proxied app
    // notice that the default port is 3000, which would clash with our expressjs
    port: 4000
    
    //browser: ['google-chrome']
  });
});


/**
 * compass 用來產生sass對應出來的css語法
 */
gulp.task('compass', function() {
  gulp.src('app/assets/sass/*.scss') //來源路徑
  .pipe(compass({ //這段內輸入config.rb的內容
    css: 'assets/css', //compass輸出位置
    sass: 'assets/sass', //sass來源路徑
    sourcemap: true, //compass 1.0 sourcemap
    style: 'compact', //CSS壓縮格式，預設(nested)
    comments: false, //是否要註解，預設(true)
    require: ['susy'] //額外套件 susy
  }))
   .on('error', function(error) {
      // Would like to catch the error here 
      console.log(error);
      this.emit('end');
    })
  .pipe(minifyCSS(
      {
          noAdvanced: false,
          keepBreaks:true,
          cache: true // 這是 gulp 插件獨有的
      }
    ))
  //.pipe(gulp.dest('app/assets/temp')); //輸出位置(非必要)
});

gulp.task('watch', function () {
  gulp.watch('assets/sass/*.scss', ['compass'])
  gulp.watch('app/**/*.js', ['uglify'])
});

gulp.task('default', ['browser-sync', 'compass', 'watch', 'uglify']);